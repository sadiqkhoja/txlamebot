/**
 * Created by sadiq on 4/14/2017.
 */
module.exports = function(robot) {
    robot.respond(/quotes about (.*)/i,function(msg){
        msg.send("just a sec");
        robot.http("https://favqs.com/api/quotes/?filter=+"+msg.match[1]+"+")
            .header('Authorization', 'Token token="00c9b1803c31a39883f6638381ed0634"')
            .get()(function(err, res, body) {
                if(err){
                    console.log(err);
                }
                else{
                    var data = JSON.parse(body);
                    if(data.quotes.length > 0 && data.quotes[0].body != "No quotes found"){
                        data = data.quotes.sort(function(a,b){
                            return a.favorites_count - b.favorites_count;
                        });

                        var quotes = data[data.length-1].body + " - " + data[data.length-1].author;
                        for(var i=2;i<=data.length && i<=3;i++){
                            quotes += "\r\n\r\n" + data[data.length-i].body + " - " + data[data.length-i].author;
                        }
                        msg.send(quotes);
                    }
                    else{
                        msg.send("No quotes found :(");
                    }
                }
            });
    });
};