#targetIsBot = (msg) ->
#  return (process.env.HUBOT_SKYPE_USERNAME.indexOf(msg.match[1].substr(msg.match[1].indexOf(':')+1)) > -1)


appendAmbush = (data, toUser, message) ->
  data[toUser] = [message]

module.exports = (robot) ->
  robot.brain.on 'loaded', =>
    robot.brain.data.ambushes ||= {}

  robot.respond /ambush (.*?): (.*)/i, (msg) ->
    appendAmbush(robot.brain.data.ambushes, msg.match[1], msg.match[2])
    msg.send "Roger that!"

  robot.respond /stop ambushing (.*)/i, (msg) ->
    if (msg.message.user.id is "1" or msg.message.user.id isnt msg.match[1]) and (ambush = robot.brain.data.ambushes[msg.match[1]])
      delete robot.brain.data.ambushes[msg.match[1]]
      msg.send "Affirmative!"
    else
      msg.send "Negative!"

  robot.listen(
    (message) -> # Match function
      robot.brain.data.ambushes[message.user.id]
    (response) -> # Standard listener callback
      response.send response.match
  )

  robot.respond /stop all ambush/i, (msg) ->
    console.log msg.message.user.id
    if msg.message.user.id is "sadiqkhoja" or msg.message.user.id is "arshian1" or msg.message.user.id is "1"
      robot.brain.data.ambushes = {}
      msg.send "Fire in the hole!"
    else
      msg.send "Check your fire!"

