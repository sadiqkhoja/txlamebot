/**
 * Created by sadiq on 4/10/2017.
 */
var request = require('request');

var markAttendance = function(empId,action,callback){
  request.post('http://trm.techlogix.com/eAttendance/index.php', {form:{
    'empl':empId,
    'radio':action,
    'submit':'Submit'
  }},
    function(error,response,body){
      if (!error && response.statusCode == 200) {
        //console.log(body);
        var lines = body.split(/\r?\n/);
        for(var i=lines.length-1;i>=0;i--){
          console.log(lines[i]);
          if(lines[i].trim() == "</form>"){
            var message = lines[i+1].replace('&nbsp&nbsp','').replace('</th>','').replace('<b>','').replace('</b>','').trim();
            console.log(message);
            callback(message);
            break;
          }
        }
      }
    }
  ).auth(process.env.HUBOT_TX_USERNAME, process.env.HUBOT_TX_PASSWORD);
};



module.exports = function(robot) {
  robot.respond(/check me in \(empid=(\d+)\)/i, function(msg){
    robot.brain.set(msg.message.user.id,{'empid': msg.match[1]});
    markAttendance(msg.match[1],'OfficeIn',function(response){
      msg.send(response);
    });
  });

  robot.respond(/check me in$/i, function(msg){
    var user = robot.brain.get(msg.message.user.id);
    if(user && user.empid){
      markAttendance(user.empid,"OfficeIn",function(response){
        msg.send(response);
      });
    }
    else{
      msg.send("I don't have your Employee ID, first tell me that like 'LAME My Employee ID is 111' or say 'LAME Check me in (empid=111)'");
    }
  });

  robot.respond(/check me out \(empid=(\d+)\)/i, function(msg){
    robot.brain.set(msg.message.user.id,{'empid': msg.match[1]});
    markAttendance(msg.match[1],'OfficeOut',function(response){
      msg.send(response);
    });
  });

  robot.respond(/check me out$/i, function(msg){
    var user = robot.brain.get(msg.message.user.id);
    if(user && user.empid){
      markAttendance(user.empid,"OfficeOut",function(response){
        msg.send(response);
      });
    }
    else{
      msg.send("I don't have your Employee ID, first tell me that like 'LAME My Employee ID is 111' or say 'LAME Check me in (empid=111)'");
    }
  });

  robot.respond(/my employee id is (\d+)/i, function(msg){
    robot.brain.set(msg.message.user.id,{'empid': msg.match[1]});
    msg.send("great, I've put that in dead man's chest");
  });

  robot.respond(/what is my employee id/i, function(msg){
    var user = robot.brain.get(msg.message.user.id);
    if(user && user.empid){
      msg.send(user.empid);
    }
    else{
      msg.send("I don't have your Employee ID, you can tell me that by saying: 'LAME My Employee ID is 111'");
    }
  });

};
