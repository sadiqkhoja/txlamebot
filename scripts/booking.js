/*
lame main conf room booked from 6:00pm to 7:00pm today
lame main conf room booked from 6:00pm to 7:00pm on 05/05/17
lame main conf room booked from 6pm to 7:00pm today
lame main conf room booked from 6pm - 7:00pm today
lame main conf room booked 6pm to 7:00pm today
lame main conf room booked 6pm - 7:00pm today
lame main conf room booked from 6:00pm to 7:00pm
lame main conf room booked from 6pm to 7:00pm
lame main conf room booked from 6pm - 7:00pm
lame main conf room booked 6pm to 7:00pm
lame main conf room booked 6pm - 7:00pm
*/

var moment = require('moment');
var Conversation = require('hubot-conversation');
module.exports = function(robot) {
    var switchBoard = new Conversation(robot);
    robot.brain.on('loaded', function() {
        if(!robot.brain.data.bookings){
            robot.brain.data.bookings = {};
        }
    });
    robot.hear(/(main|mini) ?(?:con|conf|conference) (?:room)? booked (?:(?:from)? (\d+(?::\d+)? ?(?:am|pm)) (?:to|-) (\d+(?::\d+)? ?(?:am|pm)))((?: (?:on|for))? ?(?: (today|tomorrow)|(\d+[\/\- ]\d+[\/\- ]\d+|\d+ .* \d+))?)?/i, function(msg){
        //console.log(msg.me);
		var date = getDateFromMsg(msg.match[4],msg.match[5]);
        var booking = {
            timing: getTimingFromMsg(msg.match[2],msg.match[3],date),
            room: msg.match[1].toLowerCase(),
            user: msg.message.user.id
        };
        var bookings = getBooking(date,robot);
        var collisionIndex = collision(booking,bookings);
        if(collisionIndex > -1){
            msg.reply('Sorry, your time is colliding with booking no. ' + (collisionIndex+1));
            msg.reply(makePrintable(getBooking(date,robot)));
        } else {
            bookings.push(booking);
            saveBooking(date,robot,bookings);
            msg.reply('Done');
        }
    });
    robot.respond(/show bookings? ?(?:(?:for|of) )?(?:(today|tomorrow)|(\d+[\/\- ]\d+[\/\- ]\d+|\d+ .* \d+))?/i, function(msg){
        var date = getDateFromMsg(msg.match[1],msg.match[2]);
        var bookings = getBooking(date,robot);
        msg.reply(makePrintable(bookings));
    });
    robot.respond(/cancel bookings? ?(?:(?:for|of) )?(?:(today|tomorrow)|(\d+[\/\- ]\d+[\/\- ]\d+|\d+ .* \d+))?/i, function(msg){
        var date = getDateFromMsg(msg.match[1],msg.match[2]);
        var bookings = getBooking(date,robot);
        msg.reply(makePrintable(bookings));
        if(bookings.length > 0){
            msg.reply('Which one?');
            var dialog = switchBoard.startDialog(msg);
            dialog.addChoice(/\d+/, function(msg2){
                var bookingToCancel = bookings[msg2.match[0]-1];
                if(bookingToCancel && canCancel(bookingToCancel)){
                    bookings.splice(msg2.match[0]-1, 1);
                    saveBooking(date,robot,bookings);
                    msg2.reply('Done');
                }
            });
            dialog.addChoice(/leave it/, function(msg2){
                msg2.reply('Ok');
            });
        }
    });
}

function canCancel(booking){
    return true;
}

function getBooking(date,robot){
    var dateKey = date.format('YYYYMMDD');
    var bookings = robot.brain.data.bookings[dateKey];
    return bookings ? bookings : [];
}

function saveBooking(date,robot,bookings){
    var dateKey = date.format('YYYYMMDD');
    robot.brain.data.bookings[dateKey] = bookings;
}

function makePrintable(bookings){
    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    var out = '';
    if(bookings.length > 0){
        bookings.forEach(function(booking,index){
            out += (index + 1) + '. ' + capitalizeFirstLetter(booking.room) + ' conference room booked from ' + moment(booking.timing.start).format('hh:mm a') + ' to ' + moment(booking.timing.end).format('hh:mm a') + ' by ' + booking.user + '\n'; 
        });
    } else {
        out = 'No bookings found';
    }
    return out.trim();
}

function getTimingFromMsg(msg1,msg2,date){
    var start = moment(date.format('DD/MM/YY') + ' ' + msg1, ['DD/MM/YY ha','DD/MM/YY h a','DD/MM/YY h:ma','DD/MM/YY h:m a']);
    var end = moment(date.format('DD/MM/YY') + ' ' + msg2, ['DD/MM/YY ha','DD/MM/YY h a','DD/MM/YY h:ma','DD/MM/YY h:m a']);
    return {
        start: start,
        end: end
    }
}

function getDateFromMsg(msg1,msg2){
    if(msg1){//date specified
        date = msg1.toLowerCase();
        date = date == 'today' ? moment() : date;
        date = date == 'tomorrow' ? moment().add(1,'days') : date;
    } else if (msg2) {
        date = moment(msg2,['D/M/YY','D/M/YYYY','D-M-YY','D-M-YYYY','DD MMM YY','DD MMM YYYY','DD MMMM YY','DD MMMM YYYY']);
    } else {
        date = moment();
    }
    return date;
}

function collision(booking,list){
    var collisionDetected = -1;
    list.forEach(function(value, index){
        if(collisionDetected == -1 && booking.room == value.room && isOverlap(value.timing,booking.timing)){
            collisionDetected = index;
        }
    });
    return collisionDetected;
}

function isOverlap(timing1,timing2){
    return moment(timing1.start) < moment(timing2.end) && moment(timing2.start) < moment(timing1.end); 
}