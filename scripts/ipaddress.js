/**
 * Created by sadiq on 4/9/2017.
 */
var child_process = require('child_process');
var request = require('request');

var getHostFromPing = function(ipaddress, callback){
    child_process.exec('ping -n 4 -a ' + ipaddress, function(error, stdout, stderr) {
        //console.log(stdout);

        var lines = stdout.split(/\r?\n/);
        var hostname = lines[1].split(' ')[1];
        if(lines[1].indexOf('[') == -1){
            hostname += " (not a windows device)";
        }
        var allLost = (lines[8].indexOf('100%') > -1);

        callback(hostname,allLost);
    });
};

var getMacAddress = function(ipaddress,callback){
    child_process.exec('arp -a | find "' + ipaddress + ' "', function(error, stdout, stderr) {
        //console.log(stdout);

        var terms = stdout.split(/[ ]+/);

        if(terms[1] == ipaddress){
            callback(terms[2],true);
        }
        else{
            callback('',false);
        }

    });
};

var findVendor = function(macaddress,callback){
    request('http://api.macvendors.com/'+macaddress, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            callback(body);
        }
    });
};

module.exports = function(robot) {
    robot.respond(/who has ((?!0)(?!.*\.$)((1?\d?\d|25[0-5]|2[0-4]\d)(\.|$)){4})/i, function(msg){
        msg.send("let me check");
        var ipaddress = msg.match[1];
        getHostFromPing(ipaddress,function(hostname,allLost){
            if(allLost){
                msg.send("Device seems to be offline");
            }
            else{
                msg.send("Name: " + hostname);
                getMacAddress(ipaddress,function(macaddress,found){
                    if(found){
                        msg.send("MAC Address: " + macaddress);
                        findVendor(macaddress.substr(0,8),function(vendor){
                            msg.send("Manufacturer: " + vendor);
                        });
                    }
                    else{
                        msg.send("MAC Address not found");
                    }
                });
            }
        });
    });
};


