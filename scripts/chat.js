/**
 * Created by sadiq on 4/9/2017.
 */
var dialogues = ['where are your manners?','is ki kia zarorat thi','ziada free hone ki zarorat nahi :)','boss mene aapko bola kia hai','yeh bik gaye hai gormint'];
var greetings = ["Hello",  "Hello there.",  "Hi",  "Hi there.",  "Hey",  "Good day",  "Hiya"];
var jokes = ["When my boss asked me who is the stupid one, me or him? I told him everyone knows he doesn't hire stupid people.",
"Scientists have demonstrated that cigarettes can harm your children. Fair enough, use an ashtray.",
"Standing in the park, I was wondering why a Frisbee gets larger the closer it gets. Then it hit me.",
"I want to die peacefully in my sleep, like my grandfather.. Not screaming and yelling like the passengers in his car.",
"I think my neighbor is stalking me as she's been googling my name on her computer. I saw it through my telescope last night.",
"Two wrongs don't make a right, take your parents as an example.",
"There are two rules for success: 1) Don't tell all you know.",
"Take my advice — I'm not using it."];
var sentByBot = function(msg){
  return (process.env.HUBOT_SKYPE_USERNAME.indexOf(msg.message.user.id.substr(msg.message.user.id.indexOf(':')+1)) > -1);
};
module.exports = function(robot) {
    robot.brain.on('loaded', (function(_this) {
      return function() {
        var base;
        return (base = robot.brain.data).chatEnabled || (base.chatEnabled = true);
      };
    })(this));

    robot.respond(/stop lameness/i,function(msg){
      if(msg.message.user.id == "sadiqkhoja")
        robot.brain.data.chatEnabled = false;
    });

    robot.respond(/start lameness/i,function(msg){
      if(msg.message.user.id == "sadiqkhoja")
        robot.brain.data.chatEnabled = true;
    });

    robot.respond(/where is (.*)/i,function(msg){
        if(!robot.brain.data.chatEnabled) return;
        var person = msg.match[1];
        person = person.trim().replace(/\?/g, '');
        msg.send("Let me tell you all, its not my responsibility to find out where is " + person + ".");
    });
    robot.respond(/how to (.*)/i,function(msg){
      if(!robot.brain.data.chatEnabled) return;
      var action = msg.match[1];
      action = action.trim().replace(/\?/g, '').replace(/ /g,'+');
      msg.send("Let me google that for you");
      msg.send("https://www.google.com.pk/search?q=how+to+"+action);
    });
    robot.respond(/who is /i,function(msg){
      if(!robot.brain.data.chatEnabled) return;
      msg.send("Iski aisi ke taisi");
    });
    robot.respond(/(hi|hello|hey).*/i,function(msg){
        if(!robot.brain.data.chatEnabled) return;
        msg.send(msg.random(greetings));
    });
    robot.respond(/(what|why|how)(.*) increment/i,function(msg){
      if(!robot.brain.data.chatEnabled) return;
      msg.send("Increment? its science + art");
    });
    robot.respond(/(joke|tell me a joke)/i,function(msg){
      if(!robot.brain.data.chatEnabled) return;
      msg.send(msg.random(jokes));
    });
    robot.respond(/AA$/i,function(msg){
      if(!robot.brain.data.chatEnabled) return;
      msg.send("WS");
    });
    robot.respond(/send to chitchat (.*)/i,function(msg){
      robot.messageRoom("19:I2FhZmFxdWUzMy8kYWJlNTdiMTYzZTE3YmUzOQ==@p2p.thread.skype",msg.match[1]);
    });
    robot.respond(/send to masters (.*)/i,function(msg){
      robot.messageRoom("19:04744ec6a5dd42648ccd5754a64ba409@thread.skype",msg.match[1]);
    });
    robot.catchAll(function(msg){
      if(!robot.brain.data.chatEnabled) return;
      console.log(msg.message);
      if(!sentByBot(msg) && (!msg.message.user.isGroup || (msg.message.user.isGroup && msg.message.user.addressedToMe))){
        msg.send(msg.random(dialogues));
      }
    });


};
